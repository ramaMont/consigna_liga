#language: es
Característica: Consulta de equipo
  
  @wip
  Escenario: c1 - Consulta de equipo con 2 arqueros y 30 delanteros sin potencial
    Dado que existe el equipo "Los Malos"
    Y que tiene 2 arqueros con potencial 0
    Y que tiene 30 delanteros con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 30
    Y poder defensivo es 2

  @wip
  Escenario: c2 - Consulta de equipo con 2 arqueros y 30 delanteros con potencial
    Dado que existe el equipo "Los Buenos"
    Y que tiene 2 arqueros con potencial 5
    Y que tiene 8 delanteros con potencial 4
    Y que tiene 12 mediocampistas con potencial 3
    Y que tiene 10 defensores con potencial 2
    Cuando consulto sus detalles
    Entonces poder ofensivo es 112
    Y poder defensivo es 116      

@wip
  Escenario: rocadragon1 - Consulta de equipo con 1 arquero, 20 defensores y 11 mediocampistas con potencial
    Dado que existe el equipo "Rocadragon"
    Y que tiene 1 arqueros con potencial 1
    Y que tiene 11 mediocampistas con potencial 5
    Y que tiene 20 defensores con potencial 0    
    Cuando consulto sus detalles
    Entonces poder ofensivo es 61.5
    Y poder defensivo es 82.5 

  @wip
  Escenario: rocadragon2 - Consulta de equipo con 2 arquero, 5 delantero, 15 defensores y 10 mediocampistas con potencial
    Dado que existe el equipo "Rocadragon"
    Y que tiene 2 arqueros con potencial 3
    Y que tiene 5 delanteros con potencial 1
    Y que tiene 10 mediocampistas con potencial 0
    Y que tiene 15 defensores con potencial 0    
    Cuando consulto sus detalles
    Entonces poder ofensivo es 21
    Y poder defensivo es 33  

  @wip
  Escenario: rocadragon3 - Consulta de equipo con 2 arqueros, 10 delantero, 10 defensores y 10 mediocampistas con potencial
    Dado que existe el equipo "Rocadragon"
    Y que tiene 2 arqueros con potencial 2
    Y que tiene 10 delanteros con potencial 2
    Y que tiene 10 mediocampistas con potencial 2
    Y que tiene 10 defensores con potencial 2    
    Cuando consulto sus detalles
    Entonces poder ofensivo es 79
    Y poder defensivo es 81

  @wip
  Escenario: elvalle1 - Consulta de equipo con 10 defensores sin potencial
    Dado que existe el equipo "Los Defensores"
    Y que tiene 10 defensores con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 0
    Y poder defensivo es 10

  @wip
  Escenario: elvalle2 - Consulta de equipo con 10 mediocampistas sin potencial
    Dado que existe el equipo "Los Mediocampistas"
    Y que tiene 10 mediocampistas con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 5
    Y poder defensivo es 5
  
  @wip
  Escenario: elvalle3 - Consulta de equipo con 10 delanteros sin potencial
    Dado que existe el equipo "Los Delanteros"
    Y que tiene 10 delanteros con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 10
    Y poder defensivo es 0

  @wip
  Escenario: elvalle4 - Consulta de equipo con 2 arqueros y 10 en cada posicion
    Dado que existe el equipo "El Equilibrio"
    Y que tiene 10 delanteros con potencial 0
    Y que tiene 10 mediocampistas con potencial 0
    Y que tiene 10 defensores con potencial 0
    Y que tiene 2 arqueros con potencial 0
    Cuando consulto sus detalles
    Entonces poder ofensivo es 15
    Y poder defensivo es 17
